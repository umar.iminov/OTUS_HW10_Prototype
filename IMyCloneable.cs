﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_HW10_Prototype
{
    interface IMyCloneable <T>
    {
        T Copy();
    }
}
