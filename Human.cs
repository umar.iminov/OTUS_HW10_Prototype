﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_HW10_Prototype
{
    public class Human : ICloneable, IMyCloneable<Human>
    {
        protected readonly string _name;
        protected readonly short _age;
        protected readonly string _gender;

        public Human(string name, short age, string gender)
        {
            _name = name;
            _age = age;
            _gender = gender;
        }

        public void Eat()
        {
            Console.WriteLine($"Human eating");
        }

        public void Sleep()
        {
            Console.WriteLine($"Human sleeping");
        }

        public void Drink()
        {
            Console.WriteLine($"Human drinking");
        }

        public void Walk()
        {
            Console.WriteLine($"Human walking");
        }

        public virtual object Clone()
        {
            return Copy();
        }

        public virtual Human Copy()
        {
            return new Human(_name, _age, _gender);
        }

        public override string ToString()
        {
            return $"Name = {_name} Age = {_age} Gender = {_gender}";
        }
    }
  
}
