﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_HW10_Prototype
{
    public class Specialist : Human
    {
        protected readonly string _spec;
        public Specialist(string spec, string name, short age, string gender ) : base (name, age, gender)
            {
                _spec= spec;
            }
        public virtual void FuckUp()
        {
            Console.WriteLine($"{_spec} {_name} fucked up");
        }

        public void DoWork()
        {
            Console.WriteLine($"{_spec} {_name} doing work");
        }

        public override Human Copy()
        {
            return new Specialist(_spec, _name, _age, _gender);
        }
        public override object Clone()
        {
            return Copy();
        }

        public override string ToString()
        {
            return $"Spec: {_spec} "+base.ToString();
        }

    }
}
